"""Entry point module: exec

This file should not be modified.
"""
import os


def entry_point():
    """Private entry point.

    Delete temporary files as resource and status monitoring files.

    """
    # go into the right directory to execute the script
    path = os.path.dirname(__file__)
    script = os.path.join(path, '../implementation/clear_logs/clear_logs.sh')

    # execute the script
    # status is the exit status code returned by the program
    status = os.system(script)
    if status != 0:
        raise Exception("Error occured in clear_logs.sh")

